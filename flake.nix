{
  description = "Author disambiguation";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        disambiguateEnv = pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
          editablePackageSources = { disambiguate = ./disambiguate; };
          preferWheels = true;
          extraPackages = (ps:
            with ps; [
              ipython
              python-lsp-server
              pyls-isort
              python-lsp-black
              pylsp-mypy
            ]);
          groups = [ ];
        };
        disambiguate = pkgs.poetry2nix.mkPoetryPackages { projectDir = ./.; };
      in {
        packages.disambiguate = disambiguate;
        packages.default = self.packages.${system}.disambiguate;
        devShells.default =
          pkgs.mkShell { packages = [ disambiguateEnv pkgs.poetry ]; };
      });
}
