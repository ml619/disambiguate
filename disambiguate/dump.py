import pubnet
import pickle
import igraph as ig
import os
import sys
import numpy as np
import math
import argparse
import pandas as pd
from operator import itemgetter

def log_helper(neighbors,graph):
    total = 0
    for node in neighbors:
        total+=(1/math.log(graph.vs[node].degree()))
    return total
    
def log_helper2(graph,neighbors,auth_graph):
    total = 0
    for node in neighbors:
        total+=(1/math.log(
            auth_graph.vs.select(id_eq=node,NodeType_eq=graph.start_id)[0].degree()+graph._data.vs.select(id_eq=node,NodeType_eq=graph.start_id)[0].degree()
                
            ))
    return total


def overlap_authors(author_edges,sim):
    nodes = author_edges._data.vs.select(NodeType_eq=author_edges.end_id)
    overlaps = []
    for i in range(len(nodes)):
        for j in range(i + 1, len(nodes)):
            first_node_neighbors = author_edges._data.neighbors(nodes[i])
            second_node_neighbors = author_edges._data.neighbors(nodes[j])
            # print(first_node_neighbors)
                
            all_neighbors = set(first_node_neighbors).intersection(second_node_neighbors)
                
            overlap = len(all_neighbors)
                
            if sim == "dice":
                overlap = overlap/(nodes[i].degree()+nodes[j].degree())
            elif sim == "jacc":
                overlap = overlap/(nodes[i].degree()+nodes[j].degree()-overlap)
            elif sim == "log":
                overlap = log_helper(all_neighbors,author_edges._data)
                    
            if overlap != 0:
                overlaps.append({"source":str(nodes[i]["id"]), "target": str(nodes[j]["id"]), "weight": 1/overlap})

    return overlaps

def overlap_other(other_edges,author_edges,sim):
    other_nodes = other_edges._data.vs.select(NodeType_eq=other_edges.end_id)
    author_nodes = author_edges._data.vs.select(NodeType_eq=author_edges.end_id)
    overlps = []
    for other_node in other_nodes:
        other_overlaps =[]
        for author_node in author_nodes:
            first_node_neighbors = other_edges._data.neighbors(other_node)
            second_node_neighbors = author_edges._data.neighbors(author_node)

            auth_neighbors = set()
            othe_neighbors = set()
                
            for node_index in first_node_neighbors:
                othe_neighbors.add(other_edges._data.vs[node_index]["id"])
                
                
            for node_index in second_node_neighbors:
                auth_neighbors.add(author_edges._data.vs[node_index]["id"])
                
            all_neighbors = set(auth_neighbors).intersection(
                        othe_neighbors
                )
                
            overlap = len(all_neighbors)

            if sim == "dice":
                overlap = overlap/(other_node.degree()+author_node.degree())
            elif sim == "jacc":
                overlap = overlap/(other_node.degree()+author_node.degree()-overlap)
            elif sim == "log":
                overlap = log_helper2(author_edges,all_neighbors,other_edges._data)
                
            if overlap != 0:
                other_overlaps.append((author_node["id"],overlap))
        overlps.append(other_overlaps)

    final_overlaps = []
    for other_connections in overlps:
        for i in range(len(other_connections)):
            for j in range(i+1,len(other_connections)):
                # b1 = author_edges._data.vs.select(id_eq=other_connections[i][0],NodeType_eq=author_edges.end_id)
                # b2 = author_edges._data.vs.select(id_eq=other_connections[j][0],NodeType_eq=author_edges.end_id)
                # if len(b1) < 1 or len(b2) < 1:
                #     continue
                final_overlaps.append({"source": str(other_connections[i][0]), 
                                        "target": str(other_connections[j][0]), 
                                        "weight": other_connections[i][1]+other_connections[j][1]})
        
    return final_overlaps

def to_set(element):
    tmp = set()
    tmp.add(element)
    return tmp

def check_data(graph,data_frame,node_name,number_of_splits,og_edge_num):
    node_found = 0
    data_frame.to_csv('out.csv')
    for node in graph.vs:
        ID = str(node["id"])
        if ID == node_name:
            print("found old node")
            print(node["id"])
            exit(0)
        elif node_name + "_split_" == ID[0:len(ID)-1]:
            node_found+=1
    
    if node_found != number_of_splits:
        print("incorrect number nodes found")
        print("nodes found: " + str(node_found))
        print("should have found: " + str(number_of_splits))
        print("node: " + node_name)
        exit(0)

    node_found = 0
    for index, row in data_frame.iterrows():
        if row["name"] == node_name:
            print("found old node in dataframe")
            print(row["name"])
            exit(0)
        
        elif node_name + "_split_" == row["name"][0:len(row["name"])-1]:
            node_found+=1
    

    if node_found != number_of_splits:
        print("incorrect number nodes found in dataframe")
        print("nodes found: " + str(node_found))
        print("should have found: " + str(number_of_splits))
        print("node: " + node_name)
        exit(0)

    edge_num = 0
    for edge in graph.es:
        edge_des = str(edge["Author"])
        if edge_des == node_name:
            print("found old edge")
            print(edge)
            exit(0)
        
        
        if edge_des[0:len(edge_des)-1] == (node_name+"_split_"):
            edge_num+=1

    if og_edge_num != edge_num:
        print("incorrect number edges found")
        print("edge found: " + str(edge_num))
        exit(0)
    

def get_n_highest(graph,num_nodes):
    
    num_connections = {}
    for node in graph.vs:
        if node["NodeType"] == "Author":
            num_connections[node.index] = node.degree()
    
    return dict(sorted(num_connections.items(), key=itemgetter(1), reverse=True)[:num_nodes])



def modify_graph_for_testing(graph,data_frame,num_nodes_to_split):
    #find authors with the most publictions
    n_highest = get_n_highest(graph,num_nodes_to_split)
    
    
    #edit node in graph
    edges = []
    nodes = []
    node_attrs = {"name" : [], "id" : [],"NodeType" : [] }
    edge_attrs = {"Author" : []}
    node_indexes = []
    check_info = []
    frame_data = {"name" :[], "full_name" : [], "possiable_names" : []}
    author_data = generate_node_data(data_frame,frame_data)
    
    dupes_splits = {}
    
    for node_index in n_highest:       
        neighbors = graph.neighbors(node_index)
        num_found = 0
        num_groups = math.floor(np.sqrt(len(neighbors)))
        nodes_per_group = math.ceil(len(neighbors)/num_groups)
        node = graph.vs[node_index]
        dupes_splits[str(node["id"])] = num_groups
        #add new nodes to grpah
        row = author_data[author_data['name'] == str(node["id"])]
        name = str(node["id"])
        full_name = row["full_name"].item()
        node_indexes.append(node_index)
        author_data = author_data.drop(author_data.index[author_data['name'] == str(node["id"])])
        
        for i in range(num_groups):
            nodes.append(str(node['id']) + "_split_" + str(i))
            node_attrs["id"].append(str(node['id']) + "_split_" + str(i))
            node_attrs["name"].append(str(node['id']) + "_split_" + str(i))
            node_attrs["NodeType"].append("Author")
            
            frame_data["name"].append(name + "_split_" + str(i))
            frame_data["full_name"].append(full_name)
            tmp = set()
            tmp.add(full_name)
            frame_data["possiable_names"].append(tmp)
            
        check_info.append((name,num_groups,len(neighbors)))
        
        
        #modify edges
        for neighbor in neighbors:                                                  #wrong to use id
            edges.append([str(node['id']) + "_split_" + str(num_found // nodes_per_group), str(graph.vs[neighbor]["id"])])
            edge_attrs["Author"].append(str(node['id']) + "_split_" + str(i))
            graph.vs[neighbor]["name"] = str(graph.vs[neighbor]["id"])
            
            
            num_found+=1
        
        #add new node to dataframe
        author_data = author_data.append(pd.DataFrame(frame_data))
        
        graph.delete_vertices(node_indexes)
    
    
    graph.add_vertices(n=nodes,attributes=node_attrs)
    graph.add_edges(es=edges,attributes=edge_attrs)
    
    for node in check_info:
        check_data(graph,author_data,node[0],node[1],node[2])
    
    author_nodes = author_data.to_dict(orient='records')
    return author_nodes, dupes_splits
   




def generate_node_data(author_data,frame_data):
    author_data['full_name'] = author_data['full_name'] = author_data["LastName"].astype(str) + ", " + author_data["ForeName"]
    author_data = author_data.drop('LastName', axis=1)
    author_data = author_data.drop('ForeName', axis=1)
    author_data.rename(columns={'AuthorId': 'name'},inplace=True),

    author_data["name"] = author_data["name"].apply(str)
    author_data["possiable_names"] = author_data["full_name"].apply(to_set)

    
    return author_data

def get_overlaps(graph,types_of_edges,similarity_metric):
    overlaps = []
    overlaps.append(overlap_authors(graph[("Author-Publication")],similarity_metric))
    for edge_type in types_of_edges:
        overlaps.append(overlap_other(graph[edge_type],graph[("Author-Publication")],similarity_metric))
    return overlaps

def check_for_stray_edges(author_nodes,overlaps):
    for index, overlap in enumerate(overlaps):
        for edge in overlap:
            source_good = False
            target_good = False
            edge_good = False
        for node in author_nodes:
            if node["name"] == edge["source"]:
                source_good = True
        
            if node["name"] == edge["target"]:
                target_good = True
        
            if source_good and target_good:
                edge_good = True
                break
        if not edge_good:
            print("found a stray edge in overlap: "+str(index))
            print(edge)




parser = argparse.ArgumentParser()

parser.add_argument("-s", "--sample_size", help="amount of authors to include", type=int)
parser.add_argument("-m", "--similarity_metric", help="similarity metric to use for weights")
parser.add_argument("-n", "--nodes_to_split", help="the amount of nodes to split up", type=int)

args = parser.parse_args()

types_of_edges = (
    ("Author", "Publication"),
    ("Descriptor", "Publication"),
    ("Chemical", "Publication"),
    ("Grant","Publication"),
    ("Qualifier","Publication"),
    # ("Refrence", "Publication"),
)

types_of_nodes = (
    "Author", 
    "Publication",
    "Descriptor",
    "Chemical",
    "Grant",
    "Qualifier",
    # "Refrence"
)


unfiltered_graph = pubnet.from_dir(
        graph_name="graphs",
        nodes=types_of_nodes,
        edges=types_of_edges,
        root="Publication",
        data_dir=pubnet.data.default_data_dir(),
        representation="igraph",
    )
        
        
random_nodes = unfiltered_graph["Author"].get_random(args.sample_size)
filtered_graph = unfiltered_graph.containing("Author", "AuthorId", random_nodes["AuthorId"])


author_nodes, dupes_splits  = modify_graph_for_testing(filtered_graph["Author-Publication"]._data,filtered_graph["Author"]._data,args.nodes_to_split)

num_clusters = len(author_nodes)

modified_graph = filtered_graph

overlaps = get_overlaps(modified_graph,types_of_edges[1:],args.similarity_metric)


print("number of author_nodes: "+ str(len(author_nodes)))
for index in range(len(types_of_nodes)-1):
    print("number of "+types_of_nodes[index]+" overlaps: " + str(len(overlaps[index])))

check_for_stray_edges(author_nodes,overlaps)

auth_fname = 'author_nodes_'+args.similarity_metric+"_"+str(args.sample_size)

if os.path.isfile(auth_fname):
    os.remove(auth_fname)


pickle_file = open(auth_fname, 'ab')
pickle.dump(author_nodes, pickle_file)    
pickle_file.close()
#does enumernate give 1 or 0 like this?
for index, node_type in enumerate(types_of_nodes[1:]):
    filename = node_type + "_overlaps_" + args.similarity_metric + "_" + str(args.sample_size)
    if os.path.isfile(filename):
        os.remove(filename)
    pickle_file = open(filename, 'ab')
    pickle.dump(overlaps[index], pickle_file)    
    pickle_file.close()
    

weights = []

for index in range(1,len(types_of_nodes[1:])):
    weights.append({})
    for edge in overlaps[index]:
        weights[0][max(edge["source"],edge["target"])+min(edge["source"],edge["target"])] = edge["weight"]

all_overlap = []

for i in range(len(author_nodes)):
        for j in range(i+1,len(author_nodes)):
            key = max(author_nodes[i]["name"],author_nodes[j]["name"])+min(author_nodes[i]["name"],author_nodes[j]["name"])
            list_of_indvial_weights = []
            for index in range(len(weights)):
                if key in weights[index].keys():
                    list_of_indvial_weights.append(weights[index][key])
                else:
                    list_of_indvial_weights.append(0)
            
            w = np.mean(list_of_indvial_weights)
            
            if w != 0:
                all_overlap.append({"source": author_nodes[i]["name"], "target": author_nodes[j]["name"], "weight": w})
            
            
filename = "all" + "_overlaps_" + args.similarity_metric + "_" + str(args.sample_size)
if os.path.isfile(filename):
    os.remove(filename)
pickle_file = open(filename, 'ab')
pickle.dump(overlaps[index], pickle_file)    
pickle_file.close()

filename = args.similarity_metric + "_" + str(args.sample_size)
if os.path.isfile(filename):
    os.remove(filename)
pickle_file = open(filename, 'ab')

print(dupes_splits)
pickle.dump((dupes_splits,num_clusters), pickle_file)    
pickle_file.close()


