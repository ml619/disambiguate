import pubnet
import pickle
import igraph as ig
import os
import sys
import argparse
import json

def generate_experiment_results(clusters,beta,res,splits_dupes,algo,correct_num_clusters,weight_type,filename):
    
    found_splits_dupes = {}
    for key in splits_dupes.keys():
        found_splits_dupes[key] = 0
        
    run_data = {
        "num_clusters": len(clusters),
        "correct_num_clusters": correct_num_clusters,
        "seperation_of_dupe_or_split" : splits_dupes,
        "found_seperation_of_dupe_or_split" : found_splits_dupes,
        "num_incorrect_clusters": 0,
        "cluster_correctness" : [],
        "beta" : beta,
        "res" : res,
        "algo" : algo,
        "weight_type" : weight_type,
        "node_type" : "author"
    }
    
    # print(run_data["seperation_of_dupe_or_split"])
    
    num_incorrect_clusters = 0
    split_nodes_seen = []
    for cluster in clusters:
        cluster_is_incorrect = False
        nodes_seen = {"other" : 0}
        num_of_correct_nodes = 0
        num_of_incorrect_nodes = 0
        for node in cluster.vs:
            if "split" in node["name"]:
                split_name = node["name"][0:node["name"].index("_split_")]
                if  split_name not in nodes_seen.keys():
                    run_data["found_seperation_of_dupe_or_split"][split_name]+=1
                    nodes_seen[split_name] = 0
                nodes_seen[split_name]+=1
            elif "dupe" in node["name"]:
                dupe_name = node["name"][node["name"].i:]
                if  dupe_name not in nodes_seen.keys():
                    run_data["found_seperation_of_dupe_or_split"][dupe_name]+=1
                    nodes_seen[dupe_name] = 0
                nodes_seen[dupe_name]+=1
            else:
                nodes_seen["other"]+=1        
            for counts in nodes_seen.keys():
                if (counts == "other" and nodes_seen[counts] > 1) or (counts != "other" and nodes_seen[counts] == 1):
                    num_of_incorrect_nodes+=nodes_seen[counts]
                elif counts != "other" and nodes_seen[counts] > 1:
                    num_of_correct_nodes+=nodes_seen[counts]
            
                        
        if len(nodes_seen.keys()) > 1 or nodes_seen["other"] >1:
            run_data["num_incorrect_clusters"]+=1
            run_data["cluster_correctness"].append({"correct":num_of_correct_nodes,"incorrect":num_of_incorrect_nodes})
    
    jsonname = filename+"_"+weight_type+"_"+algo+str(beta)+"_"+str(res)+"_rundata.json"
    if os.path.isfile(jsonname):
        os.remove(jsonname)
    out_file = open(jsonname, "w")
    json.dump(run_data, out_file, indent = 6)
    out_file.close()

def test_leiden(graph,type_of_overlap,splits_dupes,correct_num_clusters,filename):
    #beta = [0.01,0.02,0.03,0.04,0.05]
    # beta = [0.01]
    # res = [1.0,1.1,1.2,1.3,1.4,1.5]
    # for b in beta:
        # for r in res:
            for fun in ["CPM","modularity"]:
                clustering = graph.community_leiden(
                                    objective_function=fun,
                                    weights="weight",
                                    # resolution=r,
                                    n_iterations=-1,
                                    beta=.01
                                   )

                generate_experiment_results(clustering.subgraphs(),.01,1,splits_dupes,"leiden",correct_num_clusters,type_of_overlap,filename+"_"+fun)


def test_greedy(graph,type_of_overlap,splits_dupes,correct_num_clusters,filename):
    clustering = graph.community_fastgreedy(weights="weight")
    generate_experiment_results(clustering.subgraphs(),None,None,splits_dupes,"greedy",correct_num_clusters,type_of_overlap,filename)

def test_infomap(graph,type_of_overlap,splits_dupes,correct_num_cluster,filenames):
    clustering = graph.community_infomap(edge_weights="weight", vertex_weights=None, trials=10)
    generate_experiment_results(clustering.subgraphs(),None,None,splits_dupes,"infomap",correct_num_clusters,type_of_overlap,filename)

def test_label_propagation(graph,type_of_overlap,splits_dupes,correct_num_clusters,filename):
    clustering = graph.community_label_propagation(weights="weight", initial=None, fixed=None)
    generate_experiment_results(clustering.subgraphs(),None,None,splits_dupes,"label_propagation",correct_num_clusters,type_of_overlap,filename)


def test_leading_eigenvector(graph,type_of_overlap,splits_dupes,correct_num_clusters,filename):
    clustering = graph.community_leading_eigenvector(clusters=None, weights="weight", arpack_options=None)
    generate_experiment_results(clustering.subgraphs(),None,None,splits_dupes,"leading_eigenvector",correct_num_clusters,type_of_overlap,filename)

def test_multilevel(graph,type_of_overlap,splits_dupes,correct_num_clusters,filename):
    res = [1.0,1.1,1.2,1.3,1.4,1.5]
    for r in res:
        clustering = graph.community_multilevel(weights=weight, return_levels=False, resolution=r)
        generate_experiment_results(clustering.subgraphs(),None,r,splits_dupes,"multilevel",correct_num_clusters,type_of_overlap,filename)
 
 


parser = argparse.ArgumentParser()

parser.add_argument("-f", "--filename", help="suffix of file")
parser.add_argument("-a", "--algo", help="algorithim to run experiments with")


args = parser.parse_args()

types_of_overlaps = (
    "Publication",
    "Descriptor",
    "Chemical",
    "Grant",
    "Qualifier",
    # "Refrence",
    "all"
)

algos = {
    "leiden" : test_leiden, 
#    "edge_betweenness" : test_edge_betweenness,
    "greedy" : test_greedy,
    "info_map" : test_infomap,
    "label_propagation" : test_label_propagation,
    "leading_eigenvector" : test_leading_eigenvector,
    "multilevel" : test_multilevel,
    # "optimal" : test_optimal,
    # "spinglass" : test_spinglass
}




pickle_file = open('author_nodes_'+args.filename, 'rb')
author_nodes = pickle.load(pickle_file)
pickle_file.close()

overlaps = []
for overlap_type in types_of_overlaps:
    pickle_file = open(overlap_type+"_overlaps_"+args.filename, 'rb')
    overlap = pickle.load(pickle_file)
    pickle_file.close()
    overlaps.append(overlap)

pickle_file = open(args.filename, 'rb')
num_splits_and_correct_num_clusters = pickle.load(pickle_file)
pickle_file.close()
print(num_splits_and_correct_num_clusters)


algo = algos[args.algo]

for index in range(len(types_of_overlaps)):
    graph = ig.Graph.DictList(vertices=author_nodes,edges=overlaps[index])
    algo(graph,types_of_overlaps[index],num_splits_and_correct_num_clusters[0],num_splits_and_correct_num_clusters[1],args.filename)